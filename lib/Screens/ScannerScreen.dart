import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

import '../Functions/Drawer.dart';
import '../Functions/Utils.dart' as utils;

class ScannerScreen extends StatefulWidget {
  @override
  BarcodeState createState() => BarcodeState();
}

class BarcodeState extends State<ScannerScreen> {
  final locationfield = TextEditingController();
  final eanfield = TextEditingController();
  final tagfield = TextEditingController();
  String titlerecp = "";

  void initState() {
    super.initState();

    locationfield.text = utils.glocation;
    tagfield.text = utils.gtag;
  }

  scanBarcode() async {
    await FlutterBarcodeScanner.scanBarcode(
            "#FF00FF", "Cancel", true, ScanMode.BARCODE)
        .then((value) => utils.gcontent = value);
    print("Scanned content : " + utils.gcontent);
    if (utils.gcontent == '-1') {
      utils.message(context, "Scanning cancelled", long: true);
      return;
    }
    utils.gformat = "EAN-" + utils.gcontent.length.toString();
    utils.sendResult(context, utils.gcontent, utils.gformat, utils.glocation, utils.gtag);
  }
  
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Scanner"),
      ),
      drawer: MyDrawer(),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Padding(
            //    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            //    child: Text("Welcome to GCstar Scanner")),
            Container(
              color: Colors.lightBlue,
              child : Center(
                  child: ValueListenableBuilder(
                      valueListenable: utils.gaddress,
                      builder: (context, value, widget) {
                        return Text(
                            utils.gaddress.value,
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                                fontSize: 18,
                                height: 1.4,
                                backgroundColor: Colors.lightBlue)
                      );}
              ))
            ),
            Container(
              color: Colors.lightBlueAccent,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: ValueListenableBuilder(
                      valueListenable: utils.titleresp,
                      builder: (context, value, widget) {
                        return Padding(
                          padding:EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                          child: Text(
                            value.toString(),
                            style: TextStyle(
                              fontSize: 20,
                              height: 1.2,
                              color: Colors.indigo,
                              fontWeight: FontWeight.bold
                          )));
                  }))])),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextFormField(
                controller: locationfield,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), hintText: 'Enter a location'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextFormField(
                controller: tagfield,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), hintText: 'Enter a tag'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'please enter a tag';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextFormField(
                controller: eanfield,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), hintText: 'Enter an ISBN/EAN code'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    utils.gcontent = "";
                    utils.titleresp.value = "";
                  });
                },
                child: Text("Clear"),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          try {
            // Add data in a global variable
            titlerecp = "";
            utils.titleresp.value = "";
            utils.gcontent = "";
            utils.glocation = locationfield.text;
            utils.gtag = tagfield.text;
            utils.gcontent = eanfield.text;
            if (utils.gcontent != "") {
              utils.gformat = 'EAN-13';
              utils.sendResult(context, utils.gcontent, utils.gformat, utils.glocation, utils.gtag);
            } else {
              scanBarcode();
            }
          } catch (e) {
            print(e);
          }
        },
        tooltip: 'Barcode',
        child: Icon(Icons.camera_alt_rounded),
      ),
    );
  }
}
