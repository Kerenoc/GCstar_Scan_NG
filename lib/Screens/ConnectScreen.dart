import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';

import '../Screens/ScannerScreen.dart';
import '../Functions/Drawer.dart';
import '../Functions/Utils.dart' as utils;

class ConnectScreen extends StatefulWidget {
  @override
  SettingsApp createState() => SettingsApp();
}

class SettingsApp extends State<ConnectScreen> {
  final _formKey = GlobalKey<FormState>();
  late Future<String> ipF;
  late Future<int> portF;

  final ipfield = TextEditingController();
  final portfield = TextEditingController();

  void initState() {
    super.initState();

    utils.init();

    utils.getInfos().then((value) {
      ipfield.text = utils.gip;
      portfield.text = utils.gport.toString();
    });

  }

  void connection() async {
    // Add data in a global variable

    if (!_formKey.currentState!.validate())
      return;
    if ((utils.gip == "" && utils.gport == 0) ||
        (utils.gip != ipfield.text ||
            utils.gport != int.parse(portfield.text))) {
      // Assign IP
      utils.gip = ipfield.text;
      await utils.saveIP();
      // Assign PORT
      utils.gport = int.parse(portfield.text);
      await utils.savePort();
    }
    if (utils.gsocket != null) {
      try {
        utils.gsocket.close();
        utils.gconnect = false;
        utils.message(context, "Previous socket closed");
      } catch (e) {
        print(e);
      }
    }
    try {
      utils.message(context, "Connecting to GCstar ....");
      utils.gsocket = await Socket.connect(utils.gip, utils.gport,
          timeout: Duration(milliseconds: 3000));
      utils.gconnect = true;
      utils.listening = false;
      utils.titleresp.value = '';
      utils.message(context, "Connection succeeded", long: true);
      Navigator.push(context,
          MaterialPageRoute(
              builder: (context) => ScannerScreen()));
    } catch (e) {
      utils.gconnect = false;
      utils.message(context, "Connection failed", long: true);
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      drawer: MyDrawer(),
      body: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: ipfield,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Enter an IP address'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter an IP adress';
                  }
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextFormField(
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.done,
                controller: portfield,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), hintText: 'Enter a port'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a port';
                  }
                },
                onFieldSubmitted: (term) {
                  connection();
                }
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: ElevatedButton(
                onPressed: () {
                  connection();
                },
                child: Text("Connect"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
