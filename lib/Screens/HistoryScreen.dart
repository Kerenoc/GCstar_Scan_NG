import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import '../Functions/Drawer.dart';
import '../Functions/Utils.dart' as utils;

class HistoryScreen extends StatefulWidget {
  @override
  HistoryState createState() => HistoryState();
}

class HistoryState extends State<HistoryScreen> {
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("History"),
      ),
      drawer: MyDrawer(),
      body: Container(
        child: ValueListenableBuilder(
            valueListenable: utils.ghistorylength,
            builder: (context, value, widget) {
              return Scrollbar(
                  child: ListView.builder(
                      padding: const EdgeInsets.all(8),
                      itemCount: utils.ghistory.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                            height: 26,
                            child: Text(
                                utils.ghistory[index],
                                style: TextStyle(fontSize: 20)
                            )
                        );
                      },
                  )
              );
            },
        )
      ),
      floatingActionButton: SpeedDial(
        child: Icon(Icons.menu),
        children: [
          SpeedDialChild(
            child: Icon(Icons.sync),
            label: "Sync",
            backgroundColor: Colors.green,
            onTap: () {
              utils.resendHistory(context);
              },
          ),
          SpeedDialChild(
            child: Icon(Icons.delete),
            label: "Delete",
            backgroundColor: Colors.red,
            onTap: () async {
              utils.clearHistory();
              },
          ),
        ],
      ),
    );
  }
}
